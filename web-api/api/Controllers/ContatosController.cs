﻿using api.InputModels;
using dominio.Contatos.Entity;
using dominio.Contatos.Repository;
using dominio.Contatos.Service;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace api.Controllers
{
    [ApiController]
    [Route("api/contatos")]
    public class ContatosController : ControllerBase
    {
        readonly IContatoService contatoService;
        readonly IContatoRepository contatoRepository;
        public ContatosController(IContatoService contatoService, IContatoRepository contatoRepository)
        {
            this.contatoService = contatoService;
            this.contatoRepository = contatoRepository;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Contato), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public IActionResult Post([FromBody] CreateUpdateContato model)
        {
            Contato contato = new Contato(model.Nome, model.Sobrenome, model.Email);
            var result = contatoService.Adicionar(contato);

            if (result.IsValid)
                return Ok(contato);
            else
                return BadRequest(result);
        }

        [HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(Contato), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public IActionResult Post(Guid id, [FromBody] CreateUpdateContato model)
        {
            Contato contato = new Contato(id, model.Nome, model.Sobrenome, model.Email);
            var result = contatoService.Atualizar(contato);

            if (result.IsValid)
                return Ok(contato);
            else
                return BadRequest(result);
        }

        [HttpDelete, Route("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(Guid id)
        {
            if (contatoService.Remover(id))
                return NoContent();
            else
                return NotFound();
            
        }

        [HttpGet]
        [ProducesResponseType(typeof(Contato[]), StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            return Ok(contatoRepository.ObterTodos());
        }

        [HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(Contato[]), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(Guid id)
        {
            Contato contato = contatoRepository.Obter(id);
            if (contato == null) return NotFound();
            return Ok(contato);
        }

        [NonAction]
        public IActionResult BadRequest(ValidationResult result)
        {
            foreach (var erro in result.Errors)
                ModelState.AddModelError(erro.PropertyName, erro.ErrorMessage);

            return BadRequest(ModelState);
        }
    }
}