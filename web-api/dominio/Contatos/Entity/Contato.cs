﻿using dominio.Contatos.Validation;
using FluentValidation.Results;
using System;
using System.Runtime.Serialization;

namespace dominio.Contatos.Entity
{
    public class Contato
    {
        public Contato(string nome, string sobrenome, string email)
        {
            Id = Guid.NewGuid();
            Nome = nome;
            Sobrenome = sobrenome;
            Email = email;
        }

        public Contato(Guid id, string nome, string sobrenome, string email)
        {
            Id = id;
            Nome = nome;
            Sobrenome = sobrenome;
            Email = email;
        }

        protected Contato()
        {

        }

        public Guid Id { get; private set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Email { get; set; }
        [IgnoreDataMember]
        public ValidationResult Result { get; private set; }

        public bool EstaValido()
        {
            Result = new ContatoEstaValidoValidations().Validate(this);
            return Result.IsValid;
        }
    }
}
