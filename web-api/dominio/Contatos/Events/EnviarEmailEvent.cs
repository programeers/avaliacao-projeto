﻿namespace dominio.Contatos.Events
{
    public class EnviarEmailEvent : EventBus.Interfaces.Events.EventBase
    {
        public EnviarEmailEvent(string nome, string sobrenome, string email)
        {
            Nome = nome;
            Sobrenome = sobrenome;
            Email = email;
        }

        public string Nome { get; private set; }
        public string Sobrenome { get; private set; }
        public string Email { get; private set; }
    }
}
