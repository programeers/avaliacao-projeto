﻿using dominio.Contatos.Entity;
using FluentValidation;

namespace dominio.Contatos.Validation
{
    public class ContatoEstaValidoValidations : AbstractValidator<Contato>
    {
        public ContatoEstaValidoValidations()
        {
            RuleFor(t => t.Nome)
                .NotEmpty().WithMessage("Nome não informado")
                .MaximumLength(50).WithMessage("Quantidade não permitida. Tamanho máximo de 50 caracteres");

            RuleFor(t => t.Sobrenome)
                .NotEmpty().WithMessage("Sobrenome não informado")
                .MaximumLength(50).WithMessage("Quantidade não permitida. Tamanho máximo de 50 caracteres"); ;

            RuleFor(t => t.Email)
                .NotEmpty().WithMessage("Email não informado")
                .EmailAddress().WithMessage("Email inválido")
                .MaximumLength(200).WithMessage("Quantidade não permitida. Tamanho máximo de 200 caracteres"); ;
        }
    }
}
