# API para gravação de dados
Simples API utilizando docker, rabbitmq, banco de dados sql server e integração com gitlabci

## Subir o projeto
Configure o docker-compose com o servidor de email

```
 background-work:
    build:
      context: ./works
    networks:
      - fila
    links:
      - servico-fila
    depends_on:
      - servico-fila
    environment:
      - TYPE_EXCHANGE_BUS=1
      - TIPO_BUS=rabbit
      - PASSWORD_BUS=guest
      - TYPE_SENDER_BUS=1
      - EXCHANGE_BUS=Avaliacao
      - QUEUE_BUS=EmailEvent
      - RETRY_BUS=5
      - PORT_BUS=5672
      - HOST_BUS=servico-fila
      - USER_BUS=guest
      - SERVIDOR_EMAIL=
      - PORTA_EMAIL=
      - LOGIN_EMAIL=
      - SENHA_EMAIL=
      - SSL_EMAIL=
      - EMAIL_EMAIL=
```

Suba à aplicação com o comando:
```
docker-compose up --build
```

A aplicação demora para subir. Isso depende do host que está hospedando à aplicação.