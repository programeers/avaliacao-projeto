using email_works.Domain.Emails.Events;
using email_works.Infra.Servicos;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace email_works.Domain.Emails.EventHandlers
{
    public class EnviarEmailEventHandler : EventBus.Interfaces.EventHandlers.IEventBaseHandler<EnviarEmailEvent>
    {
        readonly IEmailService emailService;
        readonly ILogger<EnviarEmailEventHandler> logger;

        public EnviarEmailEventHandler(IEmailService emailService, ILoggerFactory loggerFactory)
        {
            this.emailService = emailService;
            this.logger = loggerFactory.CreateLogger<EnviarEmailEventHandler>();
        }

        public void Dispose()
        {
            
        }

        public async Task Handle(EnviarEmailEvent @event)
        {
            System.Console.WriteLine(@event.ToString());
            try
            {
                await emailService.EnviarEmail(@event.Email, @event.Nome, @event.Sobrenome);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, "Erro ao enviar email");
            }
        }
    }
}