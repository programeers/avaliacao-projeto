using dominio.Contatos.Entity;
using Xunit;

namespace teste
{
    public class ContatoTest
    {
        [Fact]
        public void NovoContato()
        {
            Contato contato = new Contato("Jose", "Silva", "jose.silva@gmail.com");
            Assert.True(contato.EstaValido());
        }

        [Theory]
        [InlineData("", "Silva", "jose.silva@gmail.com")]
        [InlineData("Jose", "", "jose.silva@gmail.com")]
        [InlineData("Jose", "Silva", "")]
        public void ContatoInvalido(string nome, string sobrenome, string email)
        {
            Contato contato = new Contato(nome, sobrenome, email);
            Assert.False(contato.EstaValido());
        }
    }
}
