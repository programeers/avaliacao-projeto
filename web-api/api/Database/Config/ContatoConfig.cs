﻿using dominio.Contatos.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Database.Config
{
    public class ContatoConfig : IEntityTypeConfiguration<Contato>
    {
        public void Configure(EntityTypeBuilder<Contato> builder)
        {
            builder.ToTable("Contato");

            builder.HasKey(t => t.Id);

            builder.Property(t => t.Id).IsRequired();
            builder.Property(t => t.Nome).IsRequired().HasMaxLength(50);
            builder.Property(t => t.Sobrenome).IsRequired().HasMaxLength(50);
            builder.Property(t => t.Email).IsRequired().HasMaxLength(200);

            builder.Ignore(t => t.Result);
        }
    }
}
