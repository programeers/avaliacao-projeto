namespace email_works.Domain.Emails.Events
{
    public class EnviarEmailEvent : EventBus.Interfaces.Events.EventBase
    {
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $"{Nome} - {Sobrenome} - {Email}";
        }
    }
}