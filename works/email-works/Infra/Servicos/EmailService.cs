﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace email_works.Infra.Servicos
{
    public class EmailService : IEmailService
    {
        private string servidor;
        private int porta;
        private string login;
        private string senha;
        private bool usassl;
        private string email;

        public EmailService()
        {
            servidor = Environment.GetEnvironmentVariable("SERVIDOR_EMAIL");
            int.TryParse(Environment.GetEnvironmentVariable("PORTA_EMAIL"), out int _porta);
            porta = _porta;
            login = Environment.GetEnvironmentVariable("LOGIN_EMAIL");
            senha = Environment.GetEnvironmentVariable("SENHA_EMAIL");
            bool.TryParse(Environment.GetEnvironmentVariable("SSL_EMAIL"), out bool _usassl);
            usassl = _usassl;
            email = Environment.GetEnvironmentVariable("EMAIL_EMAIL");
        }

        public async Task EnviarEmail(string destinatario, string nome, string sobrenome)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(destinatario);
            mailMessage.From = new MailAddress(email);

            string mensagem = $"<h1><strong>Bem vindo {nome} {sobrenome}</strong></h1>";
            mailMessage.Subject = "Boas vindas!";
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(mensagem, Encoding.UTF8, MediaTypeNames.Text.Html);
            mailMessage.AlternateViews.Add(avHtml);
            mailMessage.IsBodyHtml = true;

            using (var smtpClient = new SmtpClient(servidor, porta) { Credentials = new System.Net.NetworkCredential(login, senha), EnableSsl = usassl })
                await smtpClient.SendMailAsync(mailMessage);
        }
    }
}
