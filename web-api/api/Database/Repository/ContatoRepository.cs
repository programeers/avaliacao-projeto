﻿using dominio.Contatos.Entity;
using dominio.Contatos.Repository;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace api.Database.Repository
{
    public class ContatoRepository : IContatoRepository
    {
        private readonly DatabaseContext ctx;
        public ContatoRepository(DatabaseContext ctx)
        {
            this.ctx = ctx;
        }

        public bool Adicionar(Contato contato)
        {
            ctx.Contatos.Add(contato);
            return ctx.SaveChanges() > 0;
        }

        public bool Atualizar(Contato contato)
        {
            ctx.Contatos.Update(contato);
            return ctx.SaveChanges() > 0;
        }

        public void Dispose()
        {
            ctx.Dispose();
        }

        public bool Existe(Expression<Func<Contato, bool>> where)
        {
            return ctx.Contatos.Where(where).Any();
        }

        public Contato Obter(Guid id)
        {
            return ctx.Contatos.Find(id);
        }

        public Contato[] ObterTodos()
        {
            return ctx.Contatos.ToArray();
        }

        public bool Remover(Contato contato)
        {
            ctx.Contatos.Remove(contato);
            return ctx.SaveChanges() > 0;
        }
    }
}
