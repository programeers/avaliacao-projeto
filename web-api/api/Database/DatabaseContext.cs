﻿using api.Database.Config;
using dominio.Contatos.Entity;
using Microsoft.EntityFrameworkCore;

namespace api.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        public DbSet<Contato> Contatos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ContatoConfig());
        }
    }
}
