﻿using AutoMoqCore;
using dominio.Contatos.Entity;
using dominio.Contatos.Repository;
using dominio.Contatos.Service;
using Xunit;

namespace teste
{
    public class ContatoServiceTest
    {
        [Fact]
        public void IncluirContato()
        {
            AutoMoqer moqer = new AutoMoqer();
            var service = moqer.Create<ContatoService>();

            Contato contato = new Contato("jose", "silva", "jose.silva@gmail.com");
            var result = service.Adicionar(contato);
            Assert.True(result.IsValid);
        }

        [Fact]
        public void AtualizarContatoSucesso()
        {
            Contato contato = new Contato("jose", "silva", "jose.silva@gmail.com");

            AutoMoqer moqer = new AutoMoqer();
            var repositorio = moqer.GetMock<IContatoRepository>();
            repositorio.SetReturnsDefault(true);
            var service = moqer.Create<ContatoService>();

            var result = service.Atualizar(contato);
            Assert.True(result.IsValid);
        }

        [Fact]
        public void AtualizarContatoFalha()
        {
            Contato contato = new Contato("jose", "silva", "jose.silva@gmail.com");

            AutoMoqer moqer = new AutoMoqer();
            var service = moqer.Create<ContatoService>();

            var result = service.Atualizar(contato);
            Assert.False(result.IsValid);
        }


        [Fact]
        public void RemoverContato()
        {
            Contato contato = new Contato("jose", "silva", "jose.silva@gmail.com");

            AutoMoqer moqer = new AutoMoqer();
            var repositorio = moqer.GetMock<IContatoRepository>();
            repositorio.Setup(t => t.Remover(contato)).Returns(true);
            repositorio.Setup(t => t.Obter(contato.Id)).Returns(contato);
            var service = moqer.Create<ContatoService>();

            var result = service.Remover(contato.Id);
            Assert.True(result);
        }

        [Fact]
        public void RemoverContatoFalha()
        {
            Contato contato = new Contato("jose", "silva", "jose.silva@gmail.com");

            AutoMoqer moqer = new AutoMoqer();
            var service = moqer.Create<ContatoService>();

            var result = service.Remover(contato.Id);
            Assert.False(result);
        }
    }
}
