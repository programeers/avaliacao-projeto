﻿using System;
using System.IO;
using EventBus.Interfaces.Bus;
using EventBus.Interfaces.Bus.AspNetCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using email_works.Configuration;
using email_works.Domain.Emails.EventHandlers;
using email_works.Domain.Emails.Events;
using email_works.Tasks;
using email_works.Infra.Servicos;

namespace email_works
{
    class Program
    {
        static void Main(string[] args)
        {
            using (IHost host = CreateHost(args).Build())
            {
                host.Start();
                host.WaitForShutdown();
            }
        }

        private static IHostBuilder CreateHost(string[] args)
        {
            return new HostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseEnvironment(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development")
                .ConfigureAppConfiguration((ctx, configuration) =>
                {
                    configuration.AddEnvironmentVariables();
                    configuration.AddJsonFile($"appsettings.json", false, true);
                    configuration.AddJsonFile($"appsettings.{ctx.HostingEnvironment.EnvironmentName}.json", true, true);
                })
                .ConfigureLogging(logger =>
                {
                    logger.AddAzureWebAppDiagnostics();
                    logger.AddConsole();
                    logger.AddDebug();
                    logger.SetMinimumLevel(LogLevel.Trace);
                })
                .ConfigureServices((ctx, services) =>
                {
                    var configuration = ctx.Configuration;
                    services.AddOptions();

                    /*
                        Include here your services for Dependency injection
                    */
                    services.AddScoped<IEmailService, EmailService>();
                                     

                     IEventBusBuilder build = BusConfiguration.GetEventBusBuilder(services, configuration);
                    /*
                        Include here your handlers
                     */

                    build.AddSubscribe<EnviarEmailEvent, EnviarEmailEventHandler>(services);

                    services.AddSingleton(build.Build());
                    services.AddHostedService<MainTask>();
                });
        }
    }
}
