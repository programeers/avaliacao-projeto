using EventBus.Interfaces.Bus;
using Microsoft.Extensions.Options;

namespace email_works.Configuration
{
    public class ConnectionAzure : IEvenBusConnection<EventBus.AzureServiceBus.ConnectionBus>
    {
        readonly EventBus.AzureServiceBus.ConnectionBus config;
        public ConnectionAzure(IOptions<EventBus.AzureServiceBus.ConnectionBus> config)
        {
            this.config = config.Value;
        }

        public EventBus.AzureServiceBus.ConnectionBus CreateConnection()
        {
            return config;
        }
    }
}