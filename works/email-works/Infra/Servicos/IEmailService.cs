﻿using System.Threading.Tasks;

namespace email_works.Infra.Servicos
{
    public interface IEmailService
    {
        Task EnviarEmail(string destinatario, string nome, string sobrenome);
    }
}
