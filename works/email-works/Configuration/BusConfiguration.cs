using System;
using EventBus.Interfaces.Bus;
using EventBus.RabbitMQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace email_works.Configuration
{
    public class BusConfiguration
    {
        /// <summary>
        /// Class managment for providers
        /// </summary>
        internal static IEventBusBuilder GetEventBusBuilder(IServiceCollection services, IConfiguration configuration)
        {
            string tipo = Environment.GetEnvironmentVariable("TIPO_BUS") ?? "rabbit";

            if (tipo == "rabbit")
            {
                services.Configure<ConnectionBus>(item =>
                {
                    item.Host = Environment.GetEnvironmentVariable("HOST_BUS");
                    item.Password = Environment.GetEnvironmentVariable("PASSWORD_BUS");
                    item.UserName = Environment.GetEnvironmentVariable("USER_BUS");
                    int.TryParse(Environment.GetEnvironmentVariable("PORT_BUS"), out int porta);
                    item.Port = porta;
                    int.TryParse(Environment.GetEnvironmentVariable("RETRY_BUS"), out int retry);
                    item.RetryConnection = retry;
                });
                services.Configure<QueueConfig>(item =>
                {
                    item.Exchange = Environment.GetEnvironmentVariable("EXCHANGE_BUS");
                    item.Queuename = Environment.GetEnvironmentVariable("QUEUE_BUS");
                    Enum.TryParse(Environment.GetEnvironmentVariable("TYPE_EXCHANGE_BUS"), out TypeExchange exchange);
                    item.TypeExchange = exchange;
                    Enum.TryParse(Environment.GetEnvironmentVariable("TYPE_SENDER_BUS"), out ConfigureTypeSender typeSender);
                    item.TypeSender = typeSender;
                });
                services.AddScoped<IEvenBusConnection<ConnectionBus>, ConnectionRabbit>();
                services.AddScoped<RabbitEventBusBuild>();
                return services.BuildServiceProvider().GetRequiredService<RabbitEventBusBuild>();
            }
            else
            {
                services.Configure<EventBus.AzureServiceBus.ConnectionBus>(item =>
                {
                    Enum.TryParse(Environment.GetEnvironmentVariable("TYPE_SENDER_BUS"), out EventBus.AzureServiceBus.BusType bustype);
                    item.BusType = bustype;
                    item.Endpoint = Environment.GetEnvironmentVariable("ENDPOINT_BUS");
                    item.KeyName = Environment.GetEnvironmentVariable("KEYNAME_BUS");
                    item.KeyValue = Environment.GetEnvironmentVariable("KEYVALUE_BUS");
                    item.Name = Environment.GetEnvironmentVariable("NAME_BUS");
                    item.Subscription = Environment.GetEnvironmentVariable("SUBSCRIPTION_BUS");
                });
                services.AddScoped<IEvenBusConnection<EventBus.AzureServiceBus.ConnectionBus>, ConnectionAzure>();
                services.AddScoped<EventBus.AzureServiceBus.AzureEventBusBuild>();
                return services.BuildServiceProvider().GetRequiredService<EventBus.AzureServiceBus.AzureEventBusBuild>();
            }
        }
    }
}