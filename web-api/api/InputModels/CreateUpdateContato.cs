﻿using System.ComponentModel.DataAnnotations;

namespace api.InputModels
{
    public class CreateUpdateContato
    {
        [Required(ErrorMessage = "Nome não informado")]
        [MaxLength(50, ErrorMessage = "O nome deve ter no máximo 50 caracteres")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Sobrenome não informado")]
        [MaxLength(50, ErrorMessage = "O sobrenome deve ter no máximo 50 caracteres")]
        public string Sobrenome { get; set; }
        [Required(ErrorMessage = "Email não informado")]
        [EmailAddress(ErrorMessage = "Email inválido")]
        [MaxLength(50, ErrorMessage = "O email deve ter no máximo 200 caracteres")]
        public string Email { get; set; }
    }
}
