using System;
using System.Threading;
using System.Threading.Tasks;
using EventBus.Interfaces.Bus;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace email_works.Tasks
{
    public class MainTask : IHostedService
    {
        readonly IEventBus eventBus;
        readonly IServiceProvider provider;
        readonly ILogger<MainTask> logger;

        public MainTask(IEventBus eventBus, IServiceProvider provider, ILoggerFactory loggerFactory)
        {
            this.provider = provider;
            this.eventBus = eventBus;
            this.logger = loggerFactory.CreateLogger<MainTask>();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("MainTask started");
            this.logger.LogDebug("Listner start");
            await eventBus.StartHandler(provider);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            eventBus.Dispose();
            logger.LogInformation("MainTask finish");
            return Task.CompletedTask;
        }
    }
}