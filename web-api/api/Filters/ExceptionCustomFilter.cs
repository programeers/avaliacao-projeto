﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace api.Filters
{
    public class ExceptionCustomFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var exp = context.Exception;
            if (exp is DbUpdateException dbExcepiont)
                context.Result = new JsonResult(new { error = $"Não foi possível persistir os dados" }) { StatusCode = 500 };
            else
                context.Result = new JsonResult(new { error = $"Um erro ocorreu enquanto processavamos sua solicitação. Erro: {exp.InnerException?.Message ?? exp.Message}" }) { StatusCode = 500 };
        }
    }
}
