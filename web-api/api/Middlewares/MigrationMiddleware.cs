﻿using api.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace api.Middlewares
{
    public class MigrationMiddleware
    {
        private readonly RequestDelegate next;

        public MigrationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext request)
        {
            using (var scope = request.RequestServices.CreateScope())
            using (var context = scope.ServiceProvider.GetService<DatabaseContext>())
            {
                context.Database.Migrate();
            }

            return next(request);
        }
    }

    public static class MigrationMiddlewareExtension
    {
        public static IApplicationBuilder UserMigrations(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MigrationMiddleware>();
        }
    }
}
