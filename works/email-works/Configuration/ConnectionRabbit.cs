using EventBus.Interfaces.Bus;
using Microsoft.Extensions.Options;

namespace email_works.Configuration
{
    public class ConnectionRabbit : IEvenBusConnection<EventBus.RabbitMQ.ConnectionBus>
    {
        readonly EventBus.RabbitMQ.ConnectionBus config;
        public ConnectionRabbit(IOptions<EventBus.RabbitMQ.ConnectionBus> config)
        {
            this.config = config.Value;
        }

        public EventBus.RabbitMQ.ConnectionBus CreateConnection()
        {
            return config;
        }
    }
}