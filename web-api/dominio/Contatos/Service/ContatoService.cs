﻿using dominio.Contatos.Entity;
using dominio.Contatos.Events;
using dominio.Contatos.Repository;
using EventBus.Interfaces.Bus;
using FluentValidation.Results;
using Microsoft.Extensions.Logging;
using System;

namespace dominio.Contatos.Service
{
    public class ContatoService : IContatoService
    {
        private readonly IContatoRepository contatoRepository;
        private readonly IEventBus bus;
        private readonly ILogger<IContatoService> logger;

        public ContatoService(IContatoRepository contatoRepository, IEventBus bus, ILoggerFactory loggerFactory)
        {
            this.bus = bus;
            this.logger = loggerFactory.CreateLogger<ContatoService>();
            this.contatoRepository = contatoRepository;
        }

        public ValidationResult Adicionar(Contato contato)
        {
            if (!contato.EstaValido())
                return contato.Result;

            if (contatoRepository.Adicionar(contato))
            {
                try
                {
                    bus.Publish(new EnviarEmailEvent(contato.Nome, contato.Sobrenome, contato.Email)).GetAwaiter().GetResult();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Erro ao enviar evento");
                }
            }

            return contato.Result;
        }

        public ValidationResult Atualizar(Contato contato)
        {
            if (!contato.EstaValido())
                return contato.Result;

            if(!contatoRepository.Existe(t=> t.Id == contato.Id))
            {
                contato.Result.Errors.Add(new ValidationFailure(nameof(Contato.Id), "Contato não existe"));
                return contato.Result;
            }

            contatoRepository.Atualizar(contato);
            return contato.Result;
        }

        public void Dispose()
        {
            contatoRepository.Dispose();
        }

        public bool Remover(Guid id)
        {
            Contato contato = contatoRepository.Obter(id);
            if (contato == null) return false;
            return contatoRepository.Remover(contato);
        }
    }
}
