﻿using EventBus.AzureServiceBus;
using EventBus.Interfaces.Bus;
using EventBus.RabbitMQ;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace api.Configuracao
{
    public static class BusExtension
    {
        public static IServiceCollection AddBus(this IServiceCollection services)
        {
            string tipo = Environment.GetEnvironmentVariable("TIPO_BUS") ?? "rabbit";

            if (!string.IsNullOrEmpty(tipo))
            {
                IEventBusBuilder builder = null;
                if (tipo == "rabbit")
                {
                    services.Configure<EventBus.RabbitMQ.ConnectionBus>(item =>
                    {
                        item.Host = Environment.GetEnvironmentVariable("HOST_BUS");
                        item.Password = Environment.GetEnvironmentVariable("PASSWORD_BUS");
                        item.UserName = Environment.GetEnvironmentVariable("USER_BUS");
                        int.TryParse(Environment.GetEnvironmentVariable("PORT_BUS"), out int porta);
                        item.Port = porta;
                        int.TryParse(Environment.GetEnvironmentVariable("RETRY_BUS"), out int retry);
                        item.RetryConnection = retry;
                    });
                    services.Configure<QueueConfig>(item =>
                    {
                        item.Exchange = Environment.GetEnvironmentVariable("EXCHANGE_BUS") ?? "";
                        item.Queuename = Environment.GetEnvironmentVariable("QUEUE_BUS") ?? "";
                        Enum.TryParse(Environment.GetEnvironmentVariable("TYPE_EXCHANGE_BUS"), out TypeExchange exchange);
                        item.TypeExchange = exchange;
                        Enum.TryParse(Environment.GetEnvironmentVariable("TYPE_SENDER_BUS"), out ConfigureTypeSender typeSender);
                        item.TypeSender = typeSender;
                    });
                    services.AddScoped<IEvenBusConnection<EventBus.RabbitMQ.ConnectionBus>, ConnectionRabbit>();
                    services.AddScoped<RabbitEventBusBuild>();
                    builder = services.BuildServiceProvider().GetRequiredService<RabbitEventBusBuild>();
                }
                else
                {
                    services.Configure<EventBus.AzureServiceBus.ConnectionBus>(item =>
                    {
                        Enum.TryParse(Environment.GetEnvironmentVariable("TYPE_SENDER_BUS"), out BusType bustype);
                        item.BusType = bustype;
                        item.Endpoint = Environment.GetEnvironmentVariable("ENDPOINT_BUS");
                        item.KeyName = Environment.GetEnvironmentVariable("KEYNAME_BUS");
                        item.KeyValue = Environment.GetEnvironmentVariable("KEYVALUE_BUS");
                        item.Name = Environment.GetEnvironmentVariable("NAME_BUS");
                        item.Subscription = Environment.GetEnvironmentVariable("SUBSCRIPTION_BUS");
                    });
                    services.AddScoped<IEvenBusConnection<EventBus.AzureServiceBus.ConnectionBus>, ConnectionAzure>();
                    services.AddScoped<AzureEventBusBuild>();
                    builder = services.BuildServiceProvider().GetRequiredService<AzureEventBusBuild>();
                }
                services.AddSingleton<IEventBus>(builder.Build());
            }
            else
            {
                services.AddScoped<IEventBus>(null);
            }
            return services;
        }
    }

    class ConnectionAzure : IEvenBusConnection<EventBus.AzureServiceBus.ConnectionBus>
    {
        readonly EventBus.AzureServiceBus.ConnectionBus config;
        public ConnectionAzure(IOptions<EventBus.AzureServiceBus.ConnectionBus> config)
        {
            this.config = config.Value;
        }

        public EventBus.AzureServiceBus.ConnectionBus CreateConnection()
        {
            return config;
        }
    }

    class ConnectionRabbit : IEvenBusConnection<EventBus.RabbitMQ.ConnectionBus>
    {
        readonly EventBus.RabbitMQ.ConnectionBus config;
        public ConnectionRabbit(IOptions<EventBus.RabbitMQ.ConnectionBus> config)
        {
            this.config = config.Value;
        }

        public EventBus.RabbitMQ.ConnectionBus CreateConnection()
        {
            return config;
        }
    }
}
