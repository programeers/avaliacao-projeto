﻿using dominio.Contatos.Entity;
using System;
using System.Linq.Expressions;

namespace dominio.Contatos.Repository
{
    public interface IContatoRepository : IDisposable
    {
        bool Adicionar(Contato contato);
        bool Atualizar(Contato contato);
        bool Remover(Contato contato);
        Contato Obter(Guid id);
        Contato[] ObterTodos();
        bool Existe(Expression<Func<Contato, bool>> where);
    }
}
