﻿using dominio.Contatos.Entity;
using FluentValidation.Results;
using System;

namespace dominio.Contatos.Service
{
    public interface IContatoService : IDisposable
    {
        ValidationResult Adicionar(Contato contato);
        ValidationResult Atualizar(Contato contato);
        bool Remover(Guid id);
        
    }
}
