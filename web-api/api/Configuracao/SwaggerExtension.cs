﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace api.Configuracao
{
    public static class SwaggerExtension
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(setup =>
            {
                setup.SwaggerDoc("v1", new Info()
                {
                    Title = "Api Avaliação",
                    Description = "Api para avaliação",
                    Version = "v1",
                    Contact = new Contact()
                    {
                        Name = "angelico",
                        Email = "angelicogfa@gmail.com"
                    }
                });
                setup.DescribeAllEnumsAsStrings();
            });
            return services;
        }
    }
}
